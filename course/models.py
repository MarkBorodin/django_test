from django.db import models


class Technology(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False)

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=32)
    technology = models.ManyToManyField(to=Technology, related_name='course')

    def __str__(self):
        return self.name
