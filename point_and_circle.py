class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __contains__(self, item):
        if isinstance(item, Point):
            if (self.x - item.x) ** 2 + (self.y - item.y) ** 2 < self.radius ** 2:
                return True
            else:
                return False


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def is_in_circle(self, point):
        if (self.x - point.x)**2 + (self.y - point.y)**2 < self.radius**2:
            return True
        else:
            return False


class Point(Shape):
    pass
