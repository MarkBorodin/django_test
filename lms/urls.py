from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from students.views import get_random, hello


urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('hello/', hello, name='hello'),
    path('password/', get_random, name='password'),
    path('students/', include('students.urls')),
    path('teachers/', include('teachers.urls')),
    path('groups/', include('groups.urls')),
    path('', include('core.urls')),
    path('accounts/', include('accounts.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
