from core.utils import parse_length_count

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupCreateForm
from groups.models import Group

from teachers.models import Teacher


def generate_groups(request):
    count = request.GET.get('count')
    if count is None:
        return HttpResponseBadRequest("count - required parameter")

    try:
        count = parse_length_count(count)
    except Exception as ex:
        return HttpResponseBadRequest(str(ex))

    teachers_list = list(Teacher.objects.all())
    for _ in range(count):
        Group.gen_group(teachers_list)

    return HttpResponse(f'added {count} groups')


class GroupListView(ListView):
    model = Group
    template_name = 'groups-list.html'
    context_object_name = 'groups'
    paginate_by = 10

    def get_queryset(self):
        groups = super().get_queryset().select_related('teacher').all()
        request = self.request

        name = request.GET.get('name')
        group_type = request.GET.get('group_type')

        if name:
            or_names = name.split('|')
            or_cond = Q()
            for or_name in or_names:
                or_cond = or_cond | Q(name=or_name)
            groups = groups.filter(or_cond)

        if group_type:
            groups = groups.filter(group_type=group_type)

        return groups


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups-create.html'
    success_url = reverse_lazy('groups:list')


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'groups-edit.html'
    context_object_name = 'group'
    success_url = reverse_lazy('groups:list')
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.all()
        return context


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('groups:list')
    pk_url_kwarg = 'id'
