from django.contrib import admin # noqa

from students.models import Student


class StudentAdmin(admin.ModelAdmin):
    exclude = ['uuid']


admin.site.register(Student, StudentAdmin)
