import random

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from faker import Faker

from teachers.models import Teacher


class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(6)]
    )

    def __str__(self):
        return f'{self.name}, {self.floor}'


class Group(models.Model):
    name = models.CharField(max_length=64, null=False)
    group_type = models.CharField(max_length=64, null=False) # noqa
    teacher = models.ForeignKey(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='groups'
    )
    head = models.OneToOneField(
        to='students.Student',
        null=True,
        on_delete=models.SET_NULL,
        related_name='head_of_group'
    )
    classrooms = models.ManyToManyField(
        to=Classroom,
        related_name='groups'
    )

    def __str__(self):
        return self.name

    @staticmethod
    def gen_group(teachers_list):
        fake = Faker()
        Group.objects.create(
            name=fake.random_int(min=0, max=100),
            type=fake.random_int(min=0, max=100),
            teacher=random.choice(teachers_list)
        )

    def clean(self):
        from students.models import Student
        st = Student.objects.get(uuid=self.head.uuid)
        if self.pk != st.group.id:
            raise ValidationError('The head must be in the group')

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)
