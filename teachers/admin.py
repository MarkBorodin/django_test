from django.contrib import admin  # noqa

from groups.models import Group

from teachers.models import Teacher


class GroupTable(admin.TabularInline):
    model = Group
    fields = ['name', 'group_type', 'head', 'classrooms']
    readonly_fields = fields
    show_change_link = True


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name']
    fields = ['first_name', 'last_name', 'birthdate']
    inlines = [GroupTable]


admin.site.register(Teacher, TeacherAdmin)
