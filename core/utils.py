import random

from faker import Faker

from groups.models import Group


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_length(request, default=10):
    length = request.GET.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def parse_length_count(count):
    if not count.isnumeric():
        raise ValueError("VALUE ERROR: int")

    count = int(count)

    if not 1 <= count <= 100:
        raise ValueError("RANGE ERROR: [1..100]")

    return count


def gen_student():
    fake = Faker()
    student = {
        'first_name': fake.first_name(),
        'last_name': fake.last_name(),
        'birthdate': fake.date(),
        'group': random.choice(list(Group.objects.all()))
    }
    return student


def format_list(lst):
    return '<br>'.join(
        str(elem)
        for elem in lst)
