from django.core.management.base import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    help = 'Create random teachers' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Количество создаваемых учителей')

    def handle(self, *args, **kwargs):
        count = kwargs['count']
        for i in range(count):
            Teacher.gen_teacher()
