import random


class Robot(object):
    max_speed = 5
    max_altitude = 100
    max_payload = 1
    turn_on = False
    _turn_off_massage = 'You have to turn on the robot!'

    def __init__(self, model):
        self.model = model
        self.power = random.randint(0, 100)

    def turn_on_the_robot(self):
        self.turn_on = True
        return print(f'{self.model} is on')

    def battle(self, robot):
        if self.turn_on is True:
            if self.power > robot.power:
                return print(f'{self.model} won!')
            else:
                return print(f'{robot.model} won!')
        else:
            return print(self._turn_off_massage)


class SpotMini(Robot):
    legs = 4

    def go(self):
        if self.turn_on is True:
            return print(f'{self.model} goes')
        else:
            return print(self._turn_off_massage)


class Handle(Robot):
    wheels = 2

    def carry_boxes(self):
        if self.turn_on is True:
            return print(f'{self.model} carries_boxes')
        else:
            return print(self._turn_off_massage)


class Atlas(Robot):
    legs = 2
    arms = 2

    def kill_the_leathers(self):
        if self.turn_on is True:
            return print(f'{self.model} kills_the_leathers')
        else:
            return print(self._turn_off_massage)
