from PIL import Image

from django.contrib.auth.models import User
from django.db import models


class UserActions(models.Model):
    class USER_ACTION(models.IntegerChoices):
        LOGIN = 0, "Login"
        LOGOUT = 1, "Logout"
        CHANGE_PASSWORD = 2, "Change Password"
        CHANGE_PROFILE = 3, "Change Profile"
        CHANGE_PROFILE_IMAGE = 4, "Change Profile Image"

    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    write_date = models.DateTimeField(auto_now_add=True)
    action = models.PositiveSmallIntegerField(choices=USER_ACTION.choices)
    info = models.CharField(max_length=128, null=True)


class Profile(models.Model):
    class IMAGE_SIZE(models.IntegerChoices):
        FIXED_SIZE = 300
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        related_name="profile"
    )
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/')
    interests = models.CharField(max_length=128, null=True)

    @staticmethod
    def resize_image(image, size=(300, 300)):
        original_image = Image.open(image)
        width, height = original_image.size

        if width > Profile.IMAGE_SIZE.FIXED_SIZE or height > Profile.IMAGE_SIZE.FIXED_SIZE:
            if width == height:
                resized_image = original_image.resize(size)
                return resized_image.save(image.file.name)
            else:
                if width > height:
                    new_width = Profile.IMAGE_SIZE.FIXED_SIZE
                    new_height = int(new_width * height / width)
                else:
                    new_height = Profile.IMAGE_SIZE.FIXED_SIZE
                    new_width = int(new_height * width / height)
                resized_image = original_image.resize((new_width, new_height), Image.ANTIALIAS)
                return resized_image.save(image.file.name)

    def save(self, *args, **kwargs):
        user = User.objects.get(username=self.user)
        try:
            if self.image != user.profile.image:
                old_image = user.profile.image
                old_image = str(old_image).split(sep='/')[1]
                new_image = self.image
                action = UserActions.USER_ACTION.CHANGE_PROFILE_IMAGE
                change_profile_image = UserActions.objects.create(
                    user=user,
                    action=action,
                    info=f'{old_image} -> {new_image}')
                change_profile_image.save()
            result = super().save()
            self.resize_image(self.image)
            return result
        except Exception: # noqa
            return super().save()
