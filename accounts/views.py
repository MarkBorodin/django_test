from accounts.forms import AccountCreateForm, AccountPasswordChangeForm, AccountProfileUpdateForm, AccountUpdateForm
from accounts.models import UserActions

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView
from django.views.generic.edit import ProcessFormView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'you have successfully registered')
        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        else:
            return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)
        messages.success(self.request, 'you have successfully logged in')
        user = form.get_user()
        action = UserActions.USER_ACTION.LOGIN
        user_is_logged = UserActions.objects.create(user=user, action=action)
        user_is_logged.save()
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def get_next_page(self):
        result = super().get_next_page()
        messages.success(self.request, 'you have successfully logged out')
        return result


class AccountUpdateView(LoginRequiredMixin, ProcessFormView):

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile
        user_form = AccountUpdateForm(data=request.POST, instance=user)
        profile_form = AccountProfileUpdateForm(data=request.POST, files=request.FILES, instance=profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )


class AccountPasswordChangeView(PasswordChangeView):
    template_name = 'password-change.html'
    form_class = AccountPasswordChangeForm
    success_url = reverse_lazy('core:index')
