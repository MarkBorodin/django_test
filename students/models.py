import uuid

from core.models import Person

from django.db import models

from groups.models import Group


class Student(Person):
    rating = models.SmallIntegerField(null=True, default=0)
    email = models.EmailField(null=True, max_length=128)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    def __str__(self):
        return f'{super().__str__()}, {self.rating}'
