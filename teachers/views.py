from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from .forms import TeacherCreateForm
from .models import Teacher


class TeacherListView(ListView):
    model = Teacher
    template_name = 'teachers-list.html'
    context_object_name = 'teachers'
    paginate_by = 10

    def get_queryset(self):
        teachers = super().get_queryset()
        request = self.request
        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')

        if first_name:
            or_names = first_name.split('|')
            or_cond = Q()
            for or_name in or_names:
                or_cond = or_cond | Q(first_name=or_name)
            teachers = teachers.filter(or_cond)

        if last_name:
            teachers = teachers.filter(last_name=last_name)

        return teachers


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers-create.html'
    success_url = reverse_lazy('teachers:list')


class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teachers-edit.html'
    context_object_name = 'teacher'
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups'] = self.get_object().groups.all()
        return context


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'id'
