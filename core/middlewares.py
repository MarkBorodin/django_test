import copy
import time
from urllib.parse import urlencode

from core.models import Logger


class PerfTrackerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        start = time.time()

        response = self.get_response(request)

        stop = time.time()

        with open('perf_log', 'a+') as f:
            elapsed_time = stop - start
            f.write(f'Execution time for {request.method} - {request.path}, {response.status_code}: {elapsed_time}s\n')

        if request.user.is_authenticated:
            log = Logger.objects.create(
                user=request.user,
                path=request.path,
                execution_time=elapsed_time,
                query_params=request.GET
            )
            log.save()

        return response


class QueryParamsInjectorMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        query_params = copy.deepcopy(request.GET)
        if 'page' in query_params:
            del query_params['page']
        request.query_params = urlencode(query_params)

        response = self.get_response(request)

        return response
