from core.utils import gen_password, gen_student, parse_length, parse_length_count

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse, HttpResponseBadRequest
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from .forms import StudentCreateForm
from .models import Student


def hello(request):
    return HttpResponse('Hello from Django!')


def get_random(request):
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)

    result = gen_password(length)

    return HttpResponse(result)


def generate_students(request):
    count = request.GET.get('count')
    if count is None:
        return HttpResponseBadRequest("count - required parameter")

    try:
        count = parse_length_count(count)
    except Exception as ex:
        return HttpResponseBadRequest(str(ex))

    for _ in range(count):
        student = gen_student()
        st = Student(**student)
        st.save()

    return HttpResponse(f'added {count} students')


class StudentListView(ListView):
    model = Student
    template_name = 'students-list.html'
    context_object_name = 'students'
    paginate_by = 10

    def get_queryset(self):
        students = super().get_queryset().select_related('group').all()
        request = self.request

        first_name = request.GET.get('first_name')
        last_name = request.GET.get('last_name')
        rating = request.GET.get('rating')

        if first_name:
            or_names = first_name.split('|')
            or_cond = Q()
            for or_name in or_names:
                or_cond = or_cond | Q(first_name=or_name)
            students = students.filter(or_cond)

        if last_name:
            students = students.filter(last_name=last_name)

        if rating:
            students = students.filter(rating=rating)

        return students


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'students-create.html'
    success_url = reverse_lazy('students:list')


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'students-edit.html'
    context_object_name = 'student'
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'uuid'


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'uuid'
