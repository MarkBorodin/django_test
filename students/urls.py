from django.urls import path

from students.views import StudentCreateView, StudentDeleteView, StudentListView, StudentUpdateView, generate_students
app_name = 'students'

urlpatterns = [
    path('', StudentListView.as_view(), name='list'),
    path('create/', StudentCreateView.as_view(), name='create'),
    path('generate-students/', generate_students, name='generate'),
    path('edit/<uuid:uuid>', StudentUpdateView.as_view(), name='edit'),
    path('delete/<uuid:uuid>', StudentDeleteView.as_view(), name='delete'),
]
