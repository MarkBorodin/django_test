from django.contrib import admin  # noqa

from groups.models import Classroom, Group

from students.models import Student


class StudentTable(admin.TabularInline):
    model = Student
    fields = ['first_name', 'last_name', 'email']
    readonly_fields = fields
    show_change_link = True


class GroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'group_type', 'head']
    fields = ['name', 'head', 'classrooms', 'teacher']
    inlines = [StudentTable]
    list_select_related = ['head']


admin.site.register(Classroom)
admin.site.register(Group, GroupAdmin)
