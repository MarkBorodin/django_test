from core.models import Person

from django.db import models

from faker import Faker


class Teacher(Person):
    specialization = models.CharField(max_length=64, null=True)

    def __str__(self):
        return f'{super().__str__()}, {self.specialization}'

    @staticmethod
    def gen_teacher():
        fake = Faker()
        Teacher.objects.create(
            firstname=fake.first_name(),
            lastname=fake.last_name(),
            birthdate=fake.date(),
        )
