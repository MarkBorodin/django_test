from django import forms
from django.core.exceptions import ValidationError

from students.models import Student


class StudentCreateForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'rating', 'email', 'phone_number', 'group']

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        if not phone_number.isnumeric():
            raise ValidationError('value error: phone number must be digits')
        return phone_number
