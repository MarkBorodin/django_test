from course.models import Course, Technology

from django.contrib import admin


class TechnologyTable(admin.TabularInline):
    model = Course.technology.through
    show_change_link = True


class CourseAdmin(admin.ModelAdmin):
    list_display = ['name']
    fields = ['name', 'technology']
    inlines = [TechnologyTable]


admin.site.register(Technology)
admin.site.register(Course, CourseAdmin)
